
$(function () {
    $(".datepicker").datepicker({
        monthNames: [
            "Styczeń", 
            "Luty", 
            "Marzec", 
            "Kwiecień", 
            "Maj", 
            "Czerwiec", 
            "Lipiec", 
            "Sierpień", 
            "Wrzesień", 
            "Pazdziernik", 
            "Listopad", 
            "Grudzień"],
            minDate: 0,
            dateFormat: 'dd-mm-yy'
    });
});


$(function () {
    $(".autocomplete").autocomplete({
        source: base_url + "/searchCities", 
        minLength: 2,
        select: function (event, ui) {
            
//            console.log(ui.item.value);
        }


    });
}); 