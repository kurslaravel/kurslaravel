@extends('layouts.backend')

@section('content')

@if( $object ?? false )
<h2>{{ __('hotel edition') }} {{ $object->name }}</h2>
@else
<h2>{{ __('Add new hotel') }}</h2>
@endif
<br />

<form method="POST" action="{{ route('saveObject', ['id'=>$object->id ?? null]) }}" {{ $novalidate }} enctype="multipart/form-data" class="form-horizontal">
    <fieldset>
        <div class="form-group">
            <label for="city" class="col-lg-2 control-label">{{ __('City') }} *</label>
            <div class="col-lg-10">
                <select name="city" class="form-control" id="city">

                    @foreach( $cities as $city )

                        @if( ( $object ?? false ) && $object->city->id == $city->id )
                            <option selected value="{{ $city->id }}">{{ $city->name }}</option>     
                        @else
                            <option value="{{ $city->id }}">{{ $city->name }}</option>       
                        @endif   

                    @endforeach       
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-lg-2 control-label">{{ __('Hotel name') }} *</label>
            <div class="col-lg-10">
                <input name="name" required type="text" value="{{ $object->name ?? old('name') }}" class="form-control" id="name" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="street" class="col-lg-2 control-label">{{ __('Street') }} *</label>
            <div class="col-lg-10">
                <input name="street" required type="text" value="{{ $object->address->street ?? old('street') }}" class="form-control" id="street" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="number" class="col-lg-2 control-label">{{ __('Number') }} *</label>
            <div class="col-lg-10">
                <input name="number" required type="number" value="{{ $object->address->number ?? old('number') }}"class="form-control" id="number" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="descr" class="col-lg-2 control-label">{{ __('Hotel description') }} *</label>
            <div class="col-lg-10">
                <textarea name="description" required class="form-control" rows="3" id="descr">{{ $object->description ?? old('description')}}</textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <label for="objectPictures">{{ __('Hotel gallery') }}</label>
                <input type="file" name="objectPictures[]" id="objectPictures" multiple>
                <p class="help-block">{{ __('Add a photo gallery of the hotel') }}</p>
            </div>
        </div>

        @if( $object ?? false )
        <div class="col-lg-10 col-lg-offset-2">

            @foreach($object->photos->chunk(4) as $chunked_photos)

                <div class="row">


                    @foreach($chunked_photos as $photo)

                        <div class="col-md-3 col-sm-6">
                            <div class="thumbnail">
                                <img class="img-responsive" src="{{ $photo->path ?? $placeholder }}" alt="...">
                                <div class="caption">
                                    <p><a href="{{ route('deletePhoto', ['id'=>$photo->id]) }}" class="btn btn-primary btn-xs" role="button">{{ __('Delete') }}</a></p>
                                </div>

                            </div>
                        </div>

                    @endforeach

                </div>


            @endforeach

        </div>

        
        @endif        

        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button type="submit" class="btn btn-primary">{{ __('Save hotel') }}</button>
            </div>
        </div>

    </fieldset>
    @csrf
</form>

@if( $object ?? false )

<hr>

<h2>{{ __('Hotel Articles') }}</h2><br />

<h3>{{ __('Add new article') }}</h3>
<form method="POST" action="{{ route('saveArticle', ['id'=>$object->id ?? null ]) }}" {{ $novalidate }} class="form-horizontal">
    <fieldset>

        <div class="form-group">
            <label for="textTitle" class="col-lg-2 control-label">{{ __('Title') }} *</label>
            <div class="col-lg-10">
                <input name="title" value="{{ old('title') }}" required type="text" class="form-control" id="textTitle" placeholder="">
            </div>
        </div>

        <div class="form-group">
            <label for="textArea" class="col-lg-2 control-label">{{ __('Content') }} *</label>
            <div class="col-lg-10">
                <textarea name="content" required class="form-control" rows="3" id="textArea">{{ old('content') }}</textarea>
                
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button type="submit" class="btn btn-primary">{{ __('Save article') }}</button>
            </div>
        </div>
    </fieldset>
    @csrf
</form>
<h3>{{ __('Articles') }}:</h3>
<div class="col-lg-10 col-lg-offset-2">

    <ul class="list-group">
        @foreach($object->articles as $article)
            <li class="list-group-item">
                {{ $article->title }} <a href="{{ route('deleteArticle', ['id'=>$article->id ]) }}">{{ __('delete') }}</a>
            </li>
        @endforeach

    </ul>
</div>

@else
<h3>{{ __('Add a hotel to be able to add an article') }}</h3>
@endif

@endsection