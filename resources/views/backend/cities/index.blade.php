@extends('layouts.backend')

@section('content')
<h1>{{ __('Cities') }}</h1>
<small>
    <a class="btn btn-success" href="{{ route('cities.create') }}" data-type="button">{{ __('Add new city') }} </a>
</small>

<div class="table-responsive">
    <table style="margin-top:15px;" class="table table-hover table-striped">
        <tr>
            <th>{{ __('City name') }}</th>
            <th>{{ __('Edit / Delete') }}</th>
        </tr>
        @foreach( $cities as $city )
            <tr>
                <td>{{ $city->name }}</td>
                <td>
                    <a href="{{ route('cities.edit',['city'=>$city->id]) }}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>

                    <form style="display: inline;" method="POST" action="{{ route('cities.destroy',['city'=>$city->id]) }}">
                        <button onclick="return confirm('Are you sure?');" class="btn btn-primary btn-xs" type="submit"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                        @method('DELETE')
                        @csrf
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
</div>

@endsection