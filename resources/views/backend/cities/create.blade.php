@extends('layouts.backend')

@section('content')

<h1>Create new city</h1>

<form method="POST" action="{{ route('cities.store') }}">
    <h3>Name * </h3>
    <input class="form-control" type="text" required name="name"><br>
    <button class="btn btn-primary" type="submit">Create</button>
    @method('POST')
    @csrf
</form>

@endsection