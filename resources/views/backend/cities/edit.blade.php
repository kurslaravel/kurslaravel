@extends('layouts.backend')

@section('content') 

<h1>Edit city</h1>
    <form method="POST" action="{{ route('cities.update', ['city'=>$city->id]) }}">
    <h3>Name * </h3>
    <input class="form-control" value="{{ $city->name }}"type="text" required name="name"><br>
    <button class="btn btn-primary" type="submit">Submit</button>
    @method('PUT')
    @csrf
</form>

@endsection