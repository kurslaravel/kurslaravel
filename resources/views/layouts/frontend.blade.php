<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>HotelsApp!</title>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://bootswatch.com/3/readable/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
     
        <script>
            var base_url = '{{ url('/') }}';
        </script>
        
    </head>
    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ route('home') }}">{{ __('Home') }}</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    @auth 
                    <ul class="nav navbar-nav navbar-right">
                        <li><p class="navbar-text">{{ __('Hello') }}</p></li>
                        <li><p class="navbar-text">{{ Auth::user()->name }}</p></li>
                        <li><a href="{{ route('adminHome') }}">Panel</a></li>
                        
                        
                        <li>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                        </li>

                    </ul>
                    @endauth
                    @guest 
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{ route('login') }}">{{ __('Sign in') }}</a></li>
                        <li><a href="{{ route('register') }}">{{ __('Sign up') }}</a></li>
                    </ul>
                    @endguest
                </div>
            </div>
        </nav>

        <div class="jumbotron">
            <div class="container">
                <h1>Hotels App!</h1>
                <p>Darmowa platforma hotelarska!</p>
                <p>Znajdz nocleg w interesującym Cie miejscu!</p>
                <form method="POST" <?php ?> action="{{ route('roomSearch') }}" class="form-inline">
                    <div class="form-group">
                        <label class="sr-only" for="city">City</label>
                        <input name="city" value="{{ old('city') }}" type="text" class="form-control autocomplete" id="city" placeholder="{{ __('City') }}">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="day_in">Check in</label>
                        <input name="check_in" value="{{ old('check_in') }}" type="text" class="form-control datepicker" id="check_in" placeholder="{{ __('Check in') }}">
                    </div>

                    <div class="form-group">
                        <label class="sr-only" for="day_out">Check out</label>
                        <input name="check_out" value="{{ old('check_out') }}" type="text" class="form-control datepicker" id="check_out" placeholder="{{ __('Check out') }}">
                    </div>
                    <div class="form-group">
                        <select name="room_size" class="form-control">
                            <option>{{ __('Room size') }}</option>
                            
                          
                            @for($i=1;$i<=5;$i++)
                                @if( old('room_size') == $i )
                                <option selected value="{{$i}}">{{$i}}</option>
                                @else
                                <option value="{{$i}}">{{$i}} {{ __('persons') }}</option>
                                @endif
                            @endfor
                            
                        </select>
                    </div>
                    <button type="submit" class="btn btn-warning">{{ __('Search') }}</button>                    
                 
                  
                {{ csrf_field() }}
                    
                </form>

            </div>
        </div>

        @yield('content')        

        <hr>

        <footer>

            <p class="text-center">&copy; 2020 HotelsApp</p>

        </footer>      

        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
       
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="{{ asset('js/app.js') }}"></script> 
        @stack('scripts')        

    </body>
</html>

