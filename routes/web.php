<?php


Route::get('/','FrontendController@index')->name('home'); 
Route::get('object'.'/{id}','FrontendController@object')->name('object');
Route::post('roomsearch','FrontendController@roomsearch')->name('roomSearch');
Route::get('room'.'/{id}','FrontendController@room')->name('room');
Route::get('article'.'/{id}','FrontendController@article')->name('article');
Route::get('person'.'/{id}','FrontendController@person')->name('person');
 
Route::get('/searchCities', 'FrontendController@searchCities');
Route::get('/ajaxGetRoomReservations/{id}', 'FrontendController@ajaxGetRoomReservations');

Route::get('/like/{likeable_id}/{type}', 'FrontendController@like')->name('like');
Route::get('/unlike/{likeable_id}/{type}', 'FrontendController@unlike')->name('unlike');

Route::post('addComment/{commentable_id}', 'FrontendController@addComment')->name('addComment');
Route::post('/makeReservation/{room_id}/{city_id}', 'FrontendController@makeReservation')->name('makeReservation');

 
Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){ 

  //for json mobile
  Route::get('/getNotifications', 'BackendController@getNotifications'); 
  Route::post('/setReadNotifications', 'BackendController@setReadNotifications'); 
    
  Route::get('/','BackendController@index')->name('adminHome'); 
  Route::get('/myobjects','BackendController@myobjects')->name('myObjects')->middleware('CheckOwner'); 
  Route::match(['GET', 'POST'], 'saveobject'.'/{id?}','BackendController@saveObject')->name('saveObject')->middleware('CheckOwner');
  Route::match(['GET', 'POST'], 'profile','BackendController@profile')->name('profile');
  Route::get('/deletePhoto/{id}', 'BackendController@deletePhoto')->name('deletePhoto');
  Route::get('/deleteArticle/{id}', 'BackendController@deleteArticle')->name('deleteArticle');
  Route::post('/saveArticle/{id?}', 'BackendController@saveArticle')->name('saveArticle');

  Route::match(['GET', 'POST'], trans('routes.saveroom').'/{id?}','BackendController@saveRoom')->name('saveRoom')->middleware('CheckOwner');
  Route::get('deleteroom'.'/{id}', 'BackendController@deleteRoom')->name('deleteRoom');
   
  Route::get('ajaxGetReservationData', 'BackendController@ajaxGetReservationData');
  Route::get('/ajaxSetReadNotification', 'BackendController@ajaxSetReadNotification');
  Route::get('/ajaxGetNotShownNotifications', 'BackendController@ajaxGetNotShownNotifications');
  Route::get('/ajaxSetShownNotifications', 'BackendController@ajaxSetShownNotifications');

  Route::get('/confirmReservation/{id}', 'BackendController@confirmReservation')->name('confirmReservation')->middleware('CheckOwner');
  Route::get('/deleteReservation/{id}', 'BackendController@deleteReservation')->name('deleteReservation'); 

  Route::resource('cities', 'CityController')->middleware('CheckAdmin');

  Route::get('deleteobject'.'/{id}','BackendController@deleteObject')->name('deleteObject')->middleware('CheckOwner');
    
});


Auth::routes();