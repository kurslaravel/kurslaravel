<?php
namespace App\HotelsApp\Presenters;


trait UserPresenter {

    public function getFullNameAttribute() {
        return $this->name.' '.$this->surname;
    }    

}