<?php


namespace App\HotelsApp\Interfaces; 


interface FrontendRepositoryInterface   {    

    public function getObjectsForMainPage();
    public function getObject($id);
  
}


